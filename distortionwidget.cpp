#include "distortionwidget.h"
#include <QPainter>

DistortionWidget::DistortionWidget(QWidget* _parent)
{
    this->setParent(_parent);
    videoImage = NULL;
    showVideo=true;
    showGridOverlay=false;
    imageWidth=width();
    imageHeight=height();
}

void DistortionWidget::clearGridPoints()
{
    gridPoints.clear();
    gridX=0;
    gridY=0;
}

void DistortionWidget::setGridSize(uint8_t d)
{
    gridX=d;
    gridY=d;
}

void DistortionWidget::addGridPoint(float x, float y)
{
    QPointF p( x * imageWidth , y * imageHeight );
    gridPoints.push_back(p);
}

void DistortionWidget::update()
{
    this->repaint();
}

void DistortionWidget::setImage(uint8_t* data,uint32_t width, uint32_t height,uint32_t channels)
{
    float imageAspect = height/(float)width;
    if (channels != 3)
    {
        printf("ERROR: displayed image must be 3-channel\n");
        exit(0);
    }
    if (videoImage && (videoImage->width() != width || videoImage->height() != height))
    {
        delete videoImage;
        videoImage=NULL;
    }
    if (videoImage == NULL)
    {
        videoImage = new QImage(width,height,QImage::Format_RGB888);
        memcpy(videoImage->scanLine(0),data,videoImage->byteCount());// sizeInBytes()); // byteCount is deprecated but want to compile w/ older Qt
    }
    else
    {
        memcpy(videoImage->scanLine(0),data,videoImage->byteCount());// sizeInBytes()); // byteCount is deprecated but want to compile w/ older Qt
        imageAspect = videoImage->height() / float(videoImage->width());
    }
    imageWidth=float(this->width());
    imageHeight=float(this->width()) * imageAspect;
    update();
}

void DistortionWidget::mousePressEvent(QMouseEvent* e)
{
    if(e->button() == Qt::MouseButton::LeftButton)
    {
        lineA = e->pos();
    }
    if(e->button() == Qt::MouseButton::RightButton)
    {
        lineB = e->pos();
    }
}

void DistortionWidget::paintEvent(QPaintEvent*)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setRenderHint(QPainter::HighQualityAntialiasing);
    float imageAspect = this->height()/this->width();
    if (showVideo && videoImage != NULL)
    {
        imageAspect = videoImage->height() / float(videoImage->width());
        painter.drawImage(0,0,videoImage->scaled(this->width(), this->width()*imageAspect));
    }

    if (showGridOverlay)
    {
        //TODO: compute these only on geometry change
        /*QPointF topl((0.0 * (width()*0.8)) +width()*0.1 ,(0.0 * (height()*0.8)) +height()*0.1);
        QPointF topr((1.0 * (width()*0.8)) +width()*0.1 ,(0.0 * (height()*0.8)) +height()*0.1);
        QPointF botl((0.0 * (width()*0.8)) +width()*0.1 ,(1.0 * (height()*0.8)) +height()*0.1);
        QPointF botr((1.0 * (width()*0.8)) +width()*0.1 ,(1.0 * (height()*0.8)) +height()*0.1);
        */

        //TODO: if we are in hmd mode, and we have a crop/center adjustment, we need to adjust our grid extents
        QPointF topl(0.0 ,0.0 );
        QPointF topr(imageWidth ,0.0);
        QPointF botl(0.0 ,imageHeight);
        QPointF botr(imageWidth,imageHeight);

        painter.fillRect(topl.x(),topl.y(),botr.x()-topl.x(),botr.y()-topl.y(),QColor(32,32,32,128));
        painter.setPen(QColor(160,160,160));
        painter.drawLine(topl,topr);
        painter.drawLine(topr,botr);
        painter.drawLine(botr,botl);
        painter.drawLine(botl,topl);

        painter.setPen(QColor(255,180,64));
        if (gridPoints.size() > 2)
        {
            QPointF p1,p2;
            for (uint32_t i=0;i<gridY;i++)
            {
            for (uint32_t j=0;j<gridX;j++)
                {
                    if (j > 0)
                    {
                        p1 = gridPoints[(j-1) + (i * gridY)];
                        p2 = gridPoints[j + (i * gridY)];
                        painter.drawLine(p1,p2);
                    }
                    if (i > 0)
                    {
                        p1 = gridPoints[j + (i-1) * gridY];
                        p2 = gridPoints[j + i * gridY];
                        painter.drawLine(p1,p2);
                    }
                }
            }
       }
    }
    painter.setPen(QColor(0,255,64));
    painter.drawLine(lineA,lineB);
}
