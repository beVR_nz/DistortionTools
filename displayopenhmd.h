#ifndef DISPLAYOPENHMD_H
#define DISPLAYOPENHMD_H

#include <QWidget>
#include <QImage>

class DisplayOpenHMD : public QWidget
{
    Q_OBJECT
    private:
        //float swapImageTimer;
        //float swapImageTimeout;
        uint32_t xOff=0;
        uint32_t yOff=0;
        QString imageName;
    public:
        void setImageName(QString s);
    public slots:
        void update();


public:
        DisplayOpenHMD(QWidget* _parent,QString _imageName);
        QImage* calibrationImage;
        void paintEvent(QPaintEvent*);
};

#endif // DISPLAYOPENHMD_H
